/* Ive modifico el programa en Junio 3 para anadir ventanas para el circulo, el corazon, y la mariposa */


#include "xyplotwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    XYPlotWindow wLine;
    XYPlotWindow wCircle;
    XYPlotWindow wHeart;
    XYPlotWindow wButterfly;
    
    double y = 0.00;
    double x = 0.00;
    double increment = 0.01;
    
    // Plot of a segment
    
    for (double t = 0; t < 2*M_PI; t = t + increment) {
        
        x = t;
        y = t;
        
        // add x and y as a point in the graph
        wLine.AddPointToGraph(x,y);
    }
    
    // After all the points have been added, plot and show the graph
    wLine.Plot();
    wLine.show();
    
    
    
    // Plot of a circle
    
    
    // YOUR CODE FOR THE CIRCLE HERE
    
    
    // Plot of a heart
    
    
    // YOUR CODE FOR THE HEART HERE
    
    
    // Plot of a butterfly
    
    
    // YOUR CODE FOR THE BUTTERFLY HERE
    
    return a.exec();
}

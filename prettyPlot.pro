#-------------------------------------------------
#
# Project created by QtCreator 2014-02-26T09:55:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = prettyPlot
TEMPLATE = app


SOURCES += main.cpp\
    qcustomplot.cpp \
    xyplotwindow.cpp

HEADERS  += \
    qcustomplot.h \
    xyplotwindow.h

FORMS    += \
    xyplotwindow.ui

OTHER_FILES +=

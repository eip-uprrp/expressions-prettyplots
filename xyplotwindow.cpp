#include "xyplotwindow.h"
#include "ui_xyplotwindow.h"
#include "qcustomplot.h"
#include <cmath>

XYPlotWindow::XYPlotWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::XYPlotWindow)
{
    ui->setupUi(this);
}


// Añade la x y y a sus respectivos vectores.
void XYPlotWindow::AddPointToGraph(double x, double y) {
        XX.push_back(x);
        YY.push_back(y);
}

// Esta función invoca los métodos apropiados de customPlot para que
// se grafiquen los puntos.
void XYPlotWindow::Plot() {
    ui->customPlot->xAxis->setLabel("x");
    ui->customPlot->yAxis->setLabel("y");
    ui->customPlot->xAxis->setRange(-25.0, 25.0);
    ui->customPlot->yAxis->setRange(-25.0, 25.0);
    
    QCPCurve *myCurve =  new QCPCurve(ui->customPlot->xAxis, ui->customPlot->yAxis);
    ui->customPlot->addPlottable(myCurve);
    myCurve->setData(XX,YY);
}

XYPlotWindow::~XYPlotWindow()
{
    delete ui;
}

